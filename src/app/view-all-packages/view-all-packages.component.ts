import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';
import { Package } from '../interface/Package';
import { AuthService } from '../services/auth.service';
import { LoaderService } from '../services/loader.service';
import { PackageService } from '../services/package.service';

@Component({
  selector: 'app-view-all-packages',
  templateUrl: './view-all-packages.component.html',
  styleUrls: ['./view-all-packages.component.scss'],
})
export class ViewAllPackagesComponent implements OnInit {
  allPackages: Package[] = [];
  place: string;
  days: any[] = [];
  nights: any[] = [];
  constructor(
    private packageService: PackageService,
    public loaderService: LoaderService,
    public authService: AuthService,
    private route: ActivatedRoute
  ) {
    this.loaderService.loader = true;
    this.place = this.route.snapshot.paramMap.get('place');
    this.packageService
      .getAllPackages()
      .pipe(take(1))
      .subscribe({
        next: (data: any) => {
          if (this.place) {
            data.forEach((packagePayload) => {
              if (packagePayload.place === this.place) {
                this.allPackages.push(packagePayload);
                this.days.push(parseInt(packagePayload.duration) + 1);
                this.nights.push(parseInt(packagePayload.duration));
              }
            });
          } else {
            this.allPackages = data;
            this.allPackages.forEach((packagePayload) => {
              this.days.push(parseInt(packagePayload.duration) + 1);
              this.nights.push(parseInt(packagePayload.duration));
            });
          }
          this.loaderService.loader = false;
        },
      });
  }
  ngOnInit() {}
}
