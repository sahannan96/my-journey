import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { MaterialComponentsModule } from './material-components.module';
import { CreatePackageComponent } from './create-package/create-package.component';
import { PackageDetailsComponent } from './package-details/package-details.component';
import { environment } from 'src/environments/environment';
import { HttpClientModule } from '@angular/common/http';
import { NgxDropzoneModule } from 'ngx-dropzone';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { LoaderComponent } from './loader/loader.component';
import { ViewAllPackagesComponent } from './view-all-packages/view-all-packages.component';
import { AuthGuard } from './services/guard/auth.guard';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CartComponent } from './cart/cart.component';
import { PaymentComponent } from './payment/payment.component';
import { AdminComponent } from './admin/admin.component';
import { NgxGalleryModule } from '@kolkov/ngx-gallery';
import { MatNativeDateModule } from '@angular/material/core';
import { ViewAllUsersComponent } from './view-all-users/view-all-users.component';
import { ViewAllOrdersComponent } from './view-all-orders/view-all-orders.component';
import { MyTripsComponent } from './my-trips/my-trips.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { NgxMaterialRatingModule } from 'ngx-material-rating';
import { ConfirmDialogComponent } from './package-details/confirm-dialog/confirm-dialog.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    CreatePackageComponent,
    PackageDetailsComponent,
    VerifyEmailComponent,
    LoaderComponent,
    ViewAllPackagesComponent,
    HeaderComponent,
    FooterComponent,
    CartComponent,
    PaymentComponent,
    AdminComponent,
    ViewAllUsersComponent,
    ViewAllOrdersComponent,
    MyTripsComponent,
    FeedbackComponent,
    ConfirmDialogComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialComponentsModule,
    HttpClientModule,
    NgxDropzoneModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    NgbModule,
    NgxGalleryModule,
    MatNativeDateModule,
    NgxMaterialRatingModule,
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent],
})
export class AppModule {}
