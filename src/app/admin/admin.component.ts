import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { take } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { PackageService } from '../services/package.service';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss'],
})
export class AdminComponent implements OnInit {
  totalPackages: Observable<any>;
  totalUsers: Observable<any>;
  totalOrders: Observable<any>;
  constructor(
    private packageService: PackageService,
    public authService: AuthService,
    private router: Router
  ) {
    if (!this.authService.userData?.isAdmin) {
      this.router.navigate(['/home']);
    }
    this.totalPackages = this.packageService.getAllPackages().pipe(take(1));

    this.totalUsers = this.packageService.getAllUsers().pipe(take(1));

    this.totalOrders = this.packageService.getAllOrders().pipe(take(1));
  }

  ngOnInit(): void {}
}
