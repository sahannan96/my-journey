import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatIconRegistry } from '@angular/material/icon';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { debounceTime, map, startWith, take } from 'rxjs/operators';
import { Package } from '../interface/Package';
import { AuthService } from '../services/auth.service';
import { LoaderService } from '../services/loader.service';
import { PackageService } from '../services/package.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  user: any;
  x1 = 0;
  x2 = 0;
  scroll1 = 0;
  scroll2 = 0;
  options = new Set();
  searchOptions: Observable<any>;
  searchList: any[] = [];
  allPackages: Package[] = [];
  popularPackages: Package[] = []
  searchForm: FormGroup;
  carousel_item = [
    {
      img: '../assets/images/andaman.png',
      name: 'Andaman',
      cost: '23,000',
    },
    {
      img: '../assets/images/goa.png',
      name: 'Goa',
      cost: '23,000',
    },
    {
      img: '../assets/images/himachal.png',
      name: 'Himachal',
      cost: '23,000',
    },
    {
      img: '../assets/images/dubai.png',
      name: 'Dubai',
      cost: '23,000',
    },
    {
      img: '../assets/images/kashmir.png',
      name: 'Kashmir',
      cost: '23,000',
    },
    {
      img: '../assets/images/maldives.png',
      name: 'Maldives',
      cost: '23,000',
    },
    {
      img: '../assets/images/corbett.png',
      name: 'Corbett',
      cost: '23,000',
    },
    {
      img: '../assets/images/coorg.png',
      name: 'Coorg',
      cost: '23,000',
    },
  ];

  constructor(
    private packageService: PackageService,
    private matIconRegistry: MatIconRegistry,
    private domSanitizer: DomSanitizer,
    public authService: AuthService,
    public loaderService: LoaderService,
    private router: Router
  ) {
    this.loaderService.loader = true;

    this.searchForm = new FormGroup({
      search: new FormControl(''),
    });

    // this.matIconRegistry.addSvgIcon(
    //   'akshardham',
    //   this.domSanitizer.bypassSecurityTrustResourceUrl(
    //     '../assets/icons/akshardham.svg'
    //   )
    // );
    // this.matIconRegistry.addSvgIcon(
    //   'de-abril-bridge',
    //   this.domSanitizer.bypassSecurityTrustResourceUrl(
    //     '../assets/icons/de-abril-bridge.svg'
    //   )
    // );

    this.packageService
      .getAllPackages()
      .pipe(take(1))
      .subscribe({
        next: (data: any) => {
          this.allPackages = data;
          this.popularPackages = this.allPackages.slice(0,12)
          this.allPackages.forEach((packagePayload) => {
            this.options.add(packagePayload.place);
          });
          this.searchList = [...this.options];
          this.loaderService.loader = false;
        },
      });

    this.searchOptions = this.searchForm.get('search').valueChanges.pipe(
      startWith(''),
      debounceTime(1000),
      map((result) =>
        result ? this.filterItems(result) : this.searchList.slice()
      )
    );
  }

  filterItems(name: string) {
    let results = this.searchList.filter(
      (item) => item.toLowerCase().indexOf(name.toLowerCase()) === 0
    );
    return results;
  }

  previous1() {
    if (this.x1 < 0) {
      this.scroll1--;
      this.x1 = this.x1 + 250;
    }
  }

  previous2() {
    if (this.x2 < 0) {
      this.scroll2--;
      this.x2 = this.x2 + 250;
    }
  }

  next1() {
    if (this.x1 > (this.allPackages.length - 1) * -110) {
      this.scroll1++;
      this.x1 = this.x1 - 250;
    }
  }

  next2() {
    if (this.x2 > (this.carousel_item.length - 1) * -110) {
      this.scroll2++;
      this.x2 = this.x2 - 250;
    }
  }

  getSearchOption() {}
  gotoPackage() {
    this.router.navigate([
      'view-all-packages',
      this.searchForm.get('search').value,
    ]);
  }
  ngOnInit() {}
}
