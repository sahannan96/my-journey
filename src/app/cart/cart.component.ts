import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Purchase } from '../interface/Purchase';
import { AuthService } from '../services/auth.service';
import { CartService } from '../services/cart.service';
import { LoaderService } from '../services/loader.service';
import { PackageService } from '../services/package.service';
import { PurchaseService } from '../services/purchase.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss'],
})
export class CartComponent implements OnInit {
  purchase: Purchase;
  days = 0;
  nights = 0;

  constructor(
    public packageService: PackageService,
    public loaderService: LoaderService,
    public purchaseService: PurchaseService,
    public authService: AuthService,
    private router: Router,
    private cartService: CartService
  ) {
    this.loaderService.loader = true;
    this.purchase = this.cartService.purchase;
    this.days = parseInt(this.purchase.package.duration) + 1;
    this.nights = parseInt(this.purchase.package.duration);
    this.loaderService.loader = false;
  }

  proceedToPay() {
    this.router.navigate(['payment']);
  }

  ngOnInit() {}
}
