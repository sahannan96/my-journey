import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';
import { Purchase } from '../interface/Purchase';
import { User } from '../interface/User';
import { AuthService } from '../services/auth.service';
import { LoaderService } from '../services/loader.service';
import { PackageService } from '../services/package.service';
import { Invoice } from '../interface/Invoice';
import pdfMake from 'pdfmake/build/pdfmake';
import pdfFonts from 'pdfmake/build/vfs_fonts';
pdfMake.vfs = pdfFonts.pdfMake.vfs;

@Component({
  selector: 'app-my-trips',
  templateUrl: './my-trips.component.html',
  styleUrls: ['./my-trips.component.scss'],
})
export class MyTripsComponent {
  myTrips: Purchase[] = [];
  documentDefinition: any;
  invoice: Invoice = {};

  constructor(
    private packageService: PackageService,
    public loaderService: LoaderService,
    public authService: AuthService,
    private route: ActivatedRoute
  ) {
    this.loaderService.loader = true;
    const uid = this.route.snapshot.paramMap.get('id');

    this.authService
      .getUserData(uid)
      .pipe(take(1))
      .subscribe({
        next: (user: User) => {
          if (user.purchase.length > 0) {
            user.purchase.forEach((purchase, index) => {
              this.packageService
                .getOrderbyId(purchase.id)
                .pipe(take(1))
                .subscribe({
                  next: (purchase: any) => {
                    this.invoice.purchase_id = purchase.id;
                    this.invoice.payment_id = purchase.id;
                    this.invoice.persons = purchase.persons;
                    this.myTrips[index] = purchase;
                  },
                });
            });

            user.purchase.forEach((purchase, index) => {
              this.packageService
                .getPackageById(purchase.package.id)
                .pipe(take(1))
                .subscribe({
                  next: (packagePayload: any) => {
                    this.myTrips[index].package = packagePayload;
                    this.invoice.package_title = packagePayload.title;
                    this.invoice.package_category = packagePayload.category;
                    this.invoice.package_description =
                      packagePayload.description;
                    this.invoice.package_amount = packagePayload.amount;
                    this.invoice.package_place = packagePayload.place;
                    this.loaderService.loader = false;
                  },
                });
            });
          } else {
            this.loaderService.loader = false;
          }
        },
      });
  }

  createPDF() {
    this.documentDefinition = this.getDocumentDefinition();
    pdfMake.createPdf(this.documentDefinition).open();
  }

  getDocumentDefinition() {
    return {
      content: [
        {
          // text: 'My Journey',
          text: 'Travel Bookers',
          bold: true,
          fontSize: 20,
          alignment: 'center',
          margin: [0, 0, 0, 20],
        },
        {
          columns: [
            [
              {
                text: 'Purchase ID : ' + this.invoice.purchase_id,
              },
              {
                text: 'Customer name : ' + this.authService.userData.name,
              },
              {
                text: 'Customer phone : ' + this.authService.userData.phone,
              },
              {
                text: 'Customer email : ' + this.authService.userData.email,
              },
              {
                text: 'Package name : ' + this.invoice.package_title,
              },
              {
                text: 'Package category : ' + this.invoice.package_category,
              },
              {
                text: 'Destination : ' + this.invoice.package_place,
              },
              {
                text: 'Package amount : ' + this.invoice.package_amount,
              },
              {
                text:
                  'Package description : ' + this.invoice.package_description,
              },
              {
                text: 'Number of persons : ' + this.invoice.persons,
              },
            ],
          ],
        },
      ],
      styles: {
        name: {
          fontSize: 16,
          bold: true,
        },
      },
    };
  }
}
