import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { Package } from '../interface/Package';
import { LoaderService } from './loader.service';
import {
  AngularFireStorage,
  AngularFireUploadTask,
} from '@angular/fire/storage';

@Injectable({
  providedIn: 'root',
})
export class PackageService {
  constructor(
    private storage: AngularFireStorage,
    public afs: AngularFirestore,
    public loaderService: LoaderService
  ) {}
  file: File[] = [];
  task: AngularFireUploadTask;

  async addPackageCategory(category) {
    this.loaderService.loader = true;
    const id = this.afs.createId();
    const packageCategory: AngularFirestoreDocument<any> = this.afs.doc(
      `package-category/${id}`
    );
    const addPackageCategory = {
      id: id,
      name: category,
    };
    return packageCategory.set(addPackageCategory).then(() => {
      this.loaderService.loader = false;
    });
  }

  getPackageCategories() {
    return this.afs.collection(`package-category`).valueChanges();
  }

  createPackage(packagePayload: Package) {
    const id = this.afs.createId();
    const packageRef: AngularFirestoreDocument<any> = this.afs.doc(
      `packages/${id}`
    );
    this.saveFilesToStorage(id, packagePayload);
    return packageRef.set(this.createObject(id, packagePayload));
  }

  getAllPackages() {
    return this.afs.collection(`packages`).valueChanges();
  }

  getPackageById(id: string) {
    return this.afs.collection('packages').doc(id).valueChanges();
  }

  getAllUsers() {
    return this.afs.collection('users').valueChanges();
  }

  getUserById(id) {
    return this.afs.collection('users').doc(id).valueChanges();
  }

  getAllOrders() {
    return this.afs.collection('orders').valueChanges();
  }

  getOrderbyId(id) {
    return this.afs.collection('orders').doc(id).valueChanges();
  }

  saveFilesToStorage(id, payload) {
    this.file = payload.files;
    const urls: any[] = [];
    for (let i = 0; i < this.file.length; i++) {
      const path = `packages/${id}/${this.file[i].name}`;
      const ref = this.storage.ref(path);
      this.task = this.storage.upload(path, this.file[i]);
      this.task.then((f) => {
        f.ref.getDownloadURL().then((url) => {
          urls.push(url);
          this.afs.collection('packages').doc(id).update({ files: urls });
        });
      });
    }
  }

  createObject(id, packagePayload) {
    const addPackage: Package = {
      id: id,
      category: packagePayload.category,
      title: packagePayload.title,
      description: packagePayload.description,
      amount: packagePayload.amount,
      schedule: packagePayload.schedule,
      place: packagePayload.place,
      duration: packagePayload.duration,
      streetView: packagePayload.streetView,
    };
    return addPackage;
  }

  deletePackage(id: string) {
    this.deleteFilesFromStorage(id);
    return this.afs.collection('packages').doc(id).delete();
  }

  deleteFilesFromStorage(id: string) {
    const path = `packages/${id}`;
    const ref = this.storage.ref(path);
    ref.listAll().subscribe((data) => {
      const promises = data.items.map((item) => {
        return item.delete();
      });
      Promise.all(promises);
    });
  }
}
