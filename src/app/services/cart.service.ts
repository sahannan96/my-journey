import { Injectable } from '@angular/core';
import { Purchase } from '../interface/Purchase';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  purchase: Purchase;
  constructor() {}

  createPurchase(purchasePayload: Purchase) {
    this.purchase = purchasePayload;
  }
}
