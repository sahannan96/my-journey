import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class LoaderService {
  loader: boolean;
  routeLoader: boolean;

  constructor() {}
}
