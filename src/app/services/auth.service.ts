import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { User } from '../interface/User';
import { LoaderService } from './loader.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  userData: User = {};

  constructor(
    private router: Router,
    public afs: AngularFirestore,
    public afAuth: AngularFireAuth,
    public loaderService: LoaderService
  ) {
    this.loaderService.loader = true;
    this.afAuth.authState.subscribe({
      next: (user) => {
        if (user) {
          this.userData.uid = user.uid;
          this.userData.emailVerified = user.emailVerified;
          this.getUserData(this.userData.uid)
            .pipe(take(1))
            .subscribe({
              next: (data: User) => {
                this.userData.name = data.name;
                this.userData.phone = data.phone;
                this.userData.email = data.email;
                localStorage.setItem('user', JSON.stringify(this.userData));
                this.userData.purchase = data.purchase;
                this.userData.isAdmin = data.isAdmin;
                this.loaderService.loader = false;
              },
            });
        } else {
          this.loaderService.loader = false;
          localStorage.setItem('user', null);
        }
      },
    });
  }

  // Sign in with email/password
  SignIn(email, password) {
    this.loaderService.loader = true;
    return this.afAuth
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        this.router.navigate(['home']);
      })
      .catch((error) => {
        this.loaderService.loader = false;
        window.alert(error.message);
      });
  }

  // Sign up with email/password
  SignUp(
    email: string,
    password: string,
    name?: string,
    phone?: number,
    isAdmin?: boolean
  ) {
    this.loaderService.loader = true;
    return this.afAuth
      .createUserWithEmailAndPassword(email, password)
      .then((result) => {
        this.SendVerificationMail();
        this.SetUserData(result.user, name, phone, isAdmin);
      })
      .catch((error) => {
        this.loaderService.loader = false;
        window.alert(error.message);
      });
  }

  // Send email verfificaiton when new user sign up
  async SendVerificationMail() {
    return (await this.afAuth.currentUser).sendEmailVerification().then(() => {
      this.loaderService.loader = false;
      this.router.navigate(['verify-email']);
    });
  }

  // Reset Forggot password
  ForgotPassword(passwordResetEmail) {
    this.loaderService.loader = true;
    return this.afAuth
      .sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        this.loaderService.loader = false;
        window.alert('Password reset email sent, check your inbox.');
      })
      .catch((error) => {
        this.loaderService.loader = false;
        window.alert(error);
      });
  }

  // Returns true when user is looged in and email is verified
  isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return user !== null && user.emailVerified !== false ? true : false;
  }

  SetUserData(user, name?, phone?, isAdmin?) {
    this.loaderService.loader = true;
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(
      `users/${user.uid}`
    );
    const userData: User = {
      name: name,
      phone: phone,
      email: user.email,
      purchase: [],
      isAdmin: isAdmin,
    };
    return userRef
      .set(userData, {
        merge: true,
      })
      .then(() => {
        this.loaderService.loader = false;
      })
      .catch((error) => {
        this.loaderService.loader = false;
        window.alert(error);
      });
  }

  getUserData(uid: string) {
    return this.afs.collection('users').doc(uid).valueChanges();
  }

  // Sign out
  SignOut() {
    this.loaderService.loader = true;
    return this.afAuth
      .signOut()
      .then(() => {
        localStorage.removeItem('user');
        this.router.navigate(['home']);
      })
      .catch((error) => {
        this.loaderService.loader = false;
        window.alert(error);
        this.router.navigate(['home']);
      });
  }
}
