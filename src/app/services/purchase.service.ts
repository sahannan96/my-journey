import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreDocument,
} from '@angular/fire/firestore';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Purchase } from '../interface/Purchase';
import { User } from '../interface/User';
import { AuthService } from './auth.service';
import { LoaderService } from './loader.service';

function _window(): any {
  return window;
}

@Injectable({
  providedIn: 'root',
})
export class PurchaseService {
  constructor(
    private authService: AuthService,
    private loaderService: LoaderService,
    public afs: AngularFirestore,
    private snackBar: MatSnackBar,
    private router: Router
  ) {}

  completePayment(purchase: Purchase) {
    this.loaderService.loader = true;

    this.createPurchase(purchase);
    this.createOrder(purchase);

    this.loaderService.loader = false;
  }

  createPurchase(purchase: Purchase) {
    purchase.id = this.afs.createId();
    this.authService.userData['purchase'].push(purchase);

    const userOrderQuery = this.afs
      .collection('users')
      .doc(this.authService.userData.uid);

    userOrderQuery
      .set(this.authService.userData, {
        merge: true,
      })
      .then(() => {})
      .catch((error) => {
        window.alert(error);
      });
  }

  createOrder(purchase: Purchase) {
    const userData: User = {
      uid: this.authService.userData.uid,
      name: this.authService.userData.name,
      phone: this.authService.userData.phone,
      email: this.authService.userData.email,
    };
    purchase.customer = userData;

    const orderQuery: AngularFirestoreDocument<any> = this.afs.doc(
      `orders/${purchase.id}`
    );

    orderQuery
      .set(purchase, {
        merge: true,
      })
      .then(() => {})
      .catch((error) => {
        window.alert(error);
      });
  }

  setFeedback(purchaseId: string, feedbackPurchase: Purchase) {
    const feedbackQuery: AngularFirestoreDocument<any> = this.afs.doc(
      `orders/${purchaseId}`
    );

    feedbackQuery
      .update(feedbackPurchase)
      .then(() => {
        this.snackBar.open('Feedback Submitted', 'Close', {
          duration: 2000,
        });
        this.router.navigate(['my-trips']);
      })
      .catch((error) => {
        window.alert(error);
      });
  }

  get nativeWindow(): any {
    return _window();
  }
}
