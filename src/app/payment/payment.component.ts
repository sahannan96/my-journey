import { Component, OnInit } from '@angular/core';
import { Package } from '../interface/Package';
import { Purchase } from '../interface/Purchase';
import { AuthService } from '../services/auth.service';
import { CartService } from '../services/cart.service';
import { LoaderService } from '../services/loader.service';
import { PackageService } from '../services/package.service';
import { PurchaseService } from '../services/purchase.service';

@Component({
  selector: 'app-payment',
  templateUrl: './payment.component.html',
  styleUrls: ['./payment.component.scss'],
})
export class PaymentComponent implements OnInit {
  options: any;
  purchase: Purchase;

  constructor(
    public packageService: PackageService,
    public loaderService: LoaderService,
    public purchaseService: PurchaseService,
    public authService: AuthService,
    private cartService: CartService
  ) {
    this.loaderService.loader = true;
    this.purchase = this.cartService.purchase;
    this.createPackageMin();
    this.initPayment();
    this.loaderService.loader = false;
  }

  ngOnInit() {}

  initPayment() {
    this.options = {
      key: 'rzp_test_f4lqtMG9S9t66H',
      amount: parseInt(this.purchase.package.amount) * 100,
      name: this.purchase.package.title,
      description: 'Test Transaction',
      handler: this.paymentHandler.bind(this),
      theme: {
        color: '#000',
      },
    };
  }

  paymentHandler(res: any) {
    this.purchase.paymentID = res.razorpay_payment_id;
    this.purchaseService.completePayment(this.purchase);
  }

  pay() {
    let rzp1 = new this.purchaseService.nativeWindow.Razorpay(this.options);
    rzp1.open();
  }

  createPackageMin() {
    const newPackage: Package = {
      id: this.purchase.package.id,
      title: this.purchase.package.title,
      description: this.purchase.package.description,
      amount: this.purchase.package.amount,
      category: this.purchase.package.category,
    };
    this.purchase.package = newPackage;
  }
}
