import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { Package } from '../interface/Package';
import { LoaderService } from '../services/loader.service';
import { PackageService } from '../services/package.service';
import {
  NgxGalleryImage,
  NgxGalleryOptions,
  NgxGalleryAnimation,
} from '@kolkov/ngx-gallery';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';
import { FormControl, FormGroup } from '@angular/forms';
import { CartService } from '../services/cart.service';
import { Purchase } from '../interface/Purchase';
import { AuthService } from '../services/auth.service';
import { MatDialog } from '@angular/material/dialog';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';

@Component({
  selector: 'app-package-details',
  templateUrl: './package-details.component.html',
  styleUrls: ['./package-details.component.scss'],
})
export class PackageDetailsComponent implements OnInit {
  currentPackage: Package;
  galleryOptions: NgxGalleryOptions[];
  galleryImages: NgxGalleryImage[] = [];
  infoForm: FormGroup;
  streetView: string = '';
  urlSafe: SafeResourceUrl;
  fromDate: Date = new Date();
  toDate: Date = new Date();
  days = 0;
  nights = 0;
  minDate: Date = new Date();

  constructor(
    private packageService: PackageService,
    public loaderService: LoaderService,
    private route: ActivatedRoute,
    private sanitizer: DomSanitizer,
    private router: Router,
    private cartService: CartService,
    public authService: AuthService,
    public dialog: MatDialog
  ) {
    this.loaderService.loader = true;
    const id = this.route.snapshot.paramMap.get('id');

    this.packageService
      .getPackageById(id)
      .pipe(take(1))
      .subscribe({
        next: (data: any) => {
          this.currentPackage = data;
          this.streetView = this.currentPackage.streetView;
          this.urlSafe = this.sanitizer.bypassSecurityTrustResourceUrl(
            this.streetView
          );
          for (let i = 0; i < this.currentPackage.files.length; i++) {
            const images = {
              small: this.currentPackage.files[i],
              medium: this.currentPackage.files[i],
              big: this.currentPackage.files[i],
            };
            this.galleryImages.push(images);
          }
          this.days = parseInt(this.currentPackage.duration) + 1;
          this.nights = parseInt(this.currentPackage.duration);
          this.loaderService.loader = false;
        },
      });
  }

  ngOnInit() {
    this.infoForm = new FormGroup({
      date: new FormControl(),
      persons: new FormControl(),
    });

    this.galleryOptions = [
      {
        width: '600px',
        height: '400px',
        thumbnails: true,
        thumbnailsColumns: 4,
        imageAutoPlay: true,
        imageAutoPlayInterval: 6000,
        arrowPrevIcon: 'fa fa-caret-left',
        arrowNextIcon: 'fa fa-caret-right',
        closeIcon: 'fa fa-close',
        imageArrows: true,
        imageAutoPlayPauseOnHover: true,
        imageArrowsAutoHide: true,
        thumbnailsArrowsAutoHide: true,
        previewCloseOnEsc: true,
        imageAnimation: NgxGalleryAnimation.Slide,
      },
      // max-width 800
      {
        breakpoint: 800,
        width: '100%',
        height: '600px',
        imagePercent: 80,
        thumbnailsPercent: 20,
        thumbnailsMargin: 20,
        thumbnailMargin: 20,
      },
      // max-width 400
      {
        breakpoint: 400,
        preview: false,
      },
    ];
  }

  setDate() {
    this.fromDate = this.infoForm.get('date').value;
    this.toDate.setDate(
      this.fromDate.getDate() + parseInt(this.currentPackage.duration)
    );
  }

  bookNow() {
    if (!this.infoForm.get('date').value) {
      this.fromDate = new Date();
      this.toDate.setDate(
        this.fromDate.getDate() + parseInt(this.currentPackage.duration)
      );
    }
    if (!this.infoForm.get('persons').value) {
      this.infoForm.get('persons').setValue('1');
    }
    this.cartService.createPurchase(this.createObject());
    this.router.navigate(['cart']);
  }

  createObject() {
    const purchase: Purchase = {
      package: this.currentPackage,
      fromDate: this.fromDate,
      toDate: this.toDate,
      persons: this.infoForm.get('persons').value,
    };
    purchase.package.amount = (
      parseInt(this.currentPackage.amount) * parseInt(purchase.persons)
    ).toString();
    return purchase;
  }

  confirmDelete() {
    const dialogRef = this.dialog.open(ConfirmDialogComponent, {
      data: this.currentPackage,
    });
  }
}
