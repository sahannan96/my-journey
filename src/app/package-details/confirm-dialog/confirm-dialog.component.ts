import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Package } from 'src/app/interface/Package';
import { LoaderService } from 'src/app/services/loader.service';
import { PackageService } from 'src/app/services/package.service';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss'],
})
export class ConfirmDialogComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: Package,
    private loaderService: LoaderService,
    private packageService: PackageService,
    private router: Router,
    private snackBar: MatSnackBar
  ) {}

  deletePackage() {
    this.loaderService.loader = true;
    this.packageService.deletePackage(this.data.id).then(() => {
      this.loaderService.loader = false;
      this.router.navigate(['view-all-packages']);
      this.snackBar.open('Package Deleted', 'Close', {
        duration: 2000,
      });
    });
  }
}
