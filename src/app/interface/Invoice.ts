export interface Invoice {
  package_title?: string;
  package_category?: string;
  package_description?: string;
  package_amount?: string;
  package_place?: string;
  purchase_id?: string;
  payment_id?: string;
  persons?: string;
}
