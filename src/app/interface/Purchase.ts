import { Package } from './Package';
import { User } from './User';

export interface Purchase {
  id?: string;
  paymentID?: string;
  package?: Package;
  fromDate?: Date;
  toDate?: Date;
  persons?: string;
  customer?: User;
  time?: Date;
  feedback?: Feedback;
}

export interface Feedback {
  rating: string;
  comment?: string;
}
