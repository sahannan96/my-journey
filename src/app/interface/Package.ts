export interface Package {
  id?: string;
  title?: string;
  category?: string;
  duration?: string;
  description?: string;
  amount?: string;
  files?: any[];
  streetView?: string;
  schedule?: any[];
  place?: string;
}
