import { Purchase } from './Purchase';

export interface User {
  uid?: string;
  name?: string;
  phone?: string;
  email?: string;
  emailVerified?: boolean;
  isAdmin?: boolean;
  purchase?: Purchase[];
}
