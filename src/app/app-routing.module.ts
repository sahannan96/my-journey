import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminComponent } from './admin/admin.component';
import { CartComponent } from './cart/cart.component';
import { CreatePackageComponent } from './create-package/create-package.component';
import { FeedbackComponent } from './feedback/feedback.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { MyTripsComponent } from './my-trips/my-trips.component';
import { PackageDetailsComponent } from './package-details/package-details.component';
import { PaymentComponent } from './payment/payment.component';
import { RegisterComponent } from './register/register.component';
import { AuthGuard } from './services/guard/auth.guard';
import { VerifyEmailComponent } from './verify-email/verify-email.component';
import { ViewAllOrdersComponent } from './view-all-orders/view-all-orders.component';
import { ViewAllPackagesComponent } from './view-all-packages/view-all-packages.component';
import { ViewAllUsersComponent } from './view-all-users/view-all-users.component';

const routes: Routes = [
  {
    path: 'my-trips/:id',
    component: MyTripsComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'feedback/:id',
    component: FeedbackComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'admin',
    component: AdminComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'view-all-users',
    component: ViewAllUsersComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'view-all-orders',
    component: ViewAllOrdersComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'payment',
    component: PaymentComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'cart',
    component: CartComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'view-all-packages',
    component: ViewAllPackagesComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'view-all-packages/:place',
    component: ViewAllPackagesComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'verify-email',
    component: VerifyEmailComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'details/:id',
    component: PackageDetailsComponent,
  },
  {
    path: 'create',
    component: CreatePackageComponent,
    canActivate: [AuthGuard],
  },
  {
    path: 'register',
    component: RegisterComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'home',
    component: HomeComponent,
  },
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full',
  },
  { path: '**', redirectTo: '/home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
