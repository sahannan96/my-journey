import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { LoaderService } from '../services/loader.service';
import { PackageService } from '../services/package.service';

@Component({
  selector: 'app-view-all-orders',
  templateUrl: './view-all-orders.component.html',
  styleUrls: ['./view-all-orders.component.scss'],
})
export class ViewAllOrdersComponent implements OnInit {
  allOrders: any[] = [];
  displayedColumns: any[] = [
    'sr_no',
    'name',
    'amount',
    'customer',
    'rating',
    'review',
  ];
  constructor(
    private packageService: PackageService,
    public loaderService: LoaderService,
    private authService: AuthService,
    private router: Router
  ) {
    if (!this.authService.userData?.isAdmin) {
      this.router.navigate(['/home']);
    }
    this.loaderService.loader = true;
    this.packageService
      .getAllOrders()
      .pipe(take(1))
      .subscribe({
        next: (data: any) => {
          this.allOrders = data;
          this.loaderService.loader = false;
        },
      });
  }

  ngOnInit(): void {}
}
