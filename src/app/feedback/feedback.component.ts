import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ThemePalette } from '@angular/material/core';
import { ActivatedRoute } from '@angular/router';
import { take } from 'rxjs/operators';
import { Package } from '../interface/Package';
import { Feedback, Purchase } from '../interface/Purchase';
import { LoaderService } from '../services/loader.service';
import { PackageService } from '../services/package.service';
import { PurchaseService } from '../services/purchase.service';

type Rating = {
  value: number;
  max: number;
  color?: ThemePalette;
  disabled?: boolean;
  dense?: boolean;
  readonly?: boolean;
};

@Component({
  selector: 'app-feedback',
  templateUrl: './feedback.component.html',
  styleUrls: ['./feedback.component.scss'],
})
export class FeedbackComponent implements OnInit {
  feedbackForm: FormGroup;
  ratings: any = {
    value: 0,
    max: 5,
    color: 'accent',
  };
  currentPackage: Package;
  purchaseId: string;

  constructor(
    public loaderService: LoaderService,
    private route: ActivatedRoute,
    private packageService: PackageService,
    private purchaseService: PurchaseService
  ) {
    const id = this.route.snapshot.paramMap.get('id');

    this.feedbackForm = new FormGroup({
      comment: new FormControl(),
    });

    this.packageService
      .getOrderbyId(id)
      .pipe(take(1))
      .subscribe({
        next: (order: Purchase) => {
          this.purchaseId = order.id;
          this.packageService
            .getPackageById(order.package.id)
            .pipe(take(1))
            .subscribe({
              next: (packagePayload: Package) => {
                this.currentPackage = packagePayload;
                this.loaderService.loader = false;
              },
            });
        },
      });
  }

  submit() {
    const feedback: Feedback = {
      rating: this.ratings.value,
      comment: this.feedbackForm.get('comment').value,
    };

    const feedbackPurchase: Purchase  = {
      feedback: feedback
    }
    this.purchaseService.setFeedback(this.purchaseId,feedbackPurchase);
  }

  ngOnInit(): void {}
}
