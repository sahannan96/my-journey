import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { debounceTime, map, startWith, take } from 'rxjs/operators';
import { Package } from '../interface/Package';
import { AuthService } from '../services/auth.service';
import { LoaderService } from '../services/loader.service';
import { PackageService } from '../services/package.service';

@Component({
  selector: 'app-create-package',
  templateUrl: './create-package.component.html',
  styleUrls: ['./create-package.component.scss'],
})
export class CreatePackageComponent implements OnInit {
  packageForm: FormGroup;
  scheduleForm: FormGroup;
  files: File[] = [];
  showAddButton: boolean = false;
  categories: Observable<any>;
  categoryList: any[] = [];
  prompt = 'Press Enter to add "';

  constructor(
    private packageService: PackageService,
    private snackBar: MatSnackBar,
    public loaderService: LoaderService,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router
  ) {
    if (!this.authService.userData?.isAdmin) {
      this.router.navigate(['/home']);
    }
  }

  ngOnInit() {
    this.createForm();
    this.createScheduleForm();
    this.getAllPackagesCategories();
    this.categories = this.packageForm.get('category').valueChanges.pipe(
      startWith(''),
      debounceTime(1000),
      map((item) => (item ? this.filterItems(item) : this.categoryList.slice()))
    );
  }

  data = {
    day: [
      {
        event: [
          {
            event: '',
          },
        ],
      },
    ],
  };

  createForm() {
    this.packageForm = this.formBuilder.group({
      title: new FormControl(),
      description: new FormControl(),
      category: new FormControl(),
      amount: new FormControl(),
      streetView: new FormControl(),
      place: new FormControl(),
      duration: new FormControl(),
    });
  }

  createScheduleForm() {
    this.scheduleForm = this.formBuilder.group({
      day: this.formBuilder.array([]),
    });
    this.setDay();
  }

  addDay() {
    let control = <FormArray>this.scheduleForm.controls.day;
    control.push(
      this.formBuilder.group({
        event: this.formBuilder.array([]),
      })
    );
  }

  deleteDay(index) {
    let control = <FormArray>this.scheduleForm.controls.day;
    control.removeAt(index);
  }

  addEvent(control) {
    control.push(
      this.formBuilder.group({
        event: [''],
      })
    );
  }

  deleteEvent(control, index) {
    control.removeAt(index);
  }

  setDay() {
    let control = <FormArray>this.scheduleForm.controls.day;
    this.data.day.forEach((x) => {
      control.push(
        this.formBuilder.group({
          event: this.setEvent(x),
        })
      );
    });
  }

  setEvent(x) {
    let arr = new FormArray([]);
    x.event.forEach((y) => {
      arr.push(
        this.formBuilder.group({
          event: y.event,
        })
      );
    });
    return arr;
  }

  onSelect(event) {
    this.files.push(...event.addedFiles);
  }

  onRemove(event) {
    this.files.splice(this.files.indexOf(event), 1);
  }

  addCategory() {
    let option = this.removePromptFromOption(
      this.packageForm.controls.category.value
    );
    if (!this.categoryList.some((entry) => entry === option)) {
      const index = this.categoryList.push(option) - 1;
      if (this.packageService.addPackageCategory(option)) {
        this.snackBar.open('Package Category Created', 'Close', {
          duration: 2000,
        });
        this.packageForm.controls.category.setValue(this.categoryList[index]);
      }
      this.getAllPackagesCategories();
    }
  }

  createPackage() {
    this.loaderService.loader = true;
    const packagePayload: Package = this.getFormValues();
    this.packageService
      .createPackage(packagePayload)
      .then(() => {
        this.clearForm();
        this.loaderService.loader = false;
        this.snackBar.open('Package Created', 'Close', { duration: 2000 });
      })
      .catch((error) => {
        this.loaderService.loader = false;
        window.alert(error.message);
      });
  }

  clearForm() {
    this.packageForm.get('title').reset();
    this.packageForm.get('description').reset();
    this.packageForm.get('category').reset();
    this.packageForm.get('amount').reset();
    this.packageForm.get('streetView').reset();
    this.packageForm.get('place').reset();
    this.packageForm.get('duration').reset();
    this.files = [];
    this.scheduleForm.reset();
  }

  filterItems(name: string) {
    let results = this.categoryList.filter(
      (item) => item.toLowerCase().indexOf(name.toLowerCase()) === 0
    );

    this.showAddButton = results.length === 0;
    if (this.showAddButton) {
      results = [this.prompt + name + '"'];
    }
    return results;
  }

  removePromptFromOption(option) {
    if (option.startsWith(this.prompt)) {
      option = option.substring(this.prompt.length, option.length - 1);
    }
    return option;
  }

  getAllPackagesCategories() {
    this.packageService
      .getPackageCategories()
      .pipe(take(1))
      .subscribe({
        next: (data: any) => {
          this.categoryList = [];
          data.forEach((element) => {
            this.categoryList.push(element.name);
          });
        },
      });
  }

  getFormValues() {
    const packagePayload: Package = {
      title: this.packageForm.get('title').value,
      category: this.packageForm.get('category').value,
      duration: this.packageForm.get('duration').value,
      description: this.packageForm.get('description').value,
      place: this.packageForm.get('place').value,
      amount: this.packageForm.get('amount').value,
      streetView: this.packageForm.get('streetView').value,
      files: this.files,
      schedule: this.scheduleForm.value,
    };
    return packagePayload;
  }
}
