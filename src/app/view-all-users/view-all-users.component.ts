import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { take } from 'rxjs/operators';
import { AuthService } from '../services/auth.service';
import { LoaderService } from '../services/loader.service';
import { PackageService } from '../services/package.service';

@Component({
  selector: 'app-view-all-users',
  templateUrl: './view-all-users.component.html',
  styleUrls: ['./view-all-users.component.scss'],
})
export class ViewAllUsersComponent implements OnInit {
  allUsers: any[] = [];
  displayedColumns: any[] = ['sr_no', 'name', 'phone', 'purchases'];
  constructor(
    private packageService: PackageService,
    public loaderService: LoaderService,
    private authService: AuthService,
    private router: Router
  ) {
    if (!this.authService.userData?.isAdmin) {
      this.router.navigate(['/home']);
    }
    this.loaderService.loader = true;
    this.packageService
      .getAllUsers()
      .pipe(take(1))
      .subscribe({
        next: (data: any) => {
          this.allUsers = data;
          this.loaderService.loader = false;
        },
      });
  }

  ngOnInit(): void {}
}
